const express = require('express');
const app = express();
const port = 3000
const cookieParser = require('cookie-parser');
const Sequelize = require('sequelize');
const bodyParser = require('body-parser');
const session = require('express-session');

let userCount = 0;


app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

app.use(cookieParser());
app.use(session({
  key: 'user_sid',
  secret: 'somerandonstuffs',
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 600000
  }
}));

app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
      res.clearCookie('user_sid');        
  }
  next();
});

var sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
      return res.redirect('/homepage');
  } else {
      next();
  }    
};

app.get('/',sessionChecker, (req, res) => {
  if(req.session.user && req.cookies.user_sid) {
    return res.redirect('/homepage');
  } else {
    return res.redirect('/login');
  }
});

app.get('/logout', (req, res) => {
  if(req.session.user && req.cookies.user_sid) {
    res.clearCookie('user_sid');
    return res.redirect('/');
  } else {
    return res.redirect('/login')
  }
});

app.get('/register', sessionChecker, (req, res) => {
  res.sendFile(__dirname + '/register.html');
})

app.get('/login', sessionChecker, (req, res) => {
  res.sendFile(__dirname + '/proiect.html');
});

app.get('/homepage', (req, res) => {
  if(req.session.user && req.cookies.user_sid) {
    res.sendFile(__dirname + '/homepage.html');
  } else {
    return res.redirect('/login');
  }
})

app.post('/loginUser', (req, res) => {
  User.findOne({
    where : {
      username: req.body.username,
      password: req.body.password
    }
  }).then(user => {
      if(!user) {
        return res.redirect('/login')
      } else {
        req.session.user = user.dataValues;
        res.redirect('/homepage');
      }
  });
});

app.post('/registerUser', (req, res) => {
  User.create({
    username: req.body.username,
    password: req.body.password
  }).then( user => {
      req.session.user = user.dataValues;
      res.redirect('/homepage');
  });
})



app.listen(port, () => console.log(`Example app listening on port ${port}!`))

const sequelize = new Sequelize('proiect_teh_web', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
  });

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

  const User = sequelize.define('user', {
    // attributes
    username: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING  ,
      allowNull: false
    }
  }


  )
User.sync({ force: true });

// const createUser = (username, password) => {
//     return User.create({
//       username: username,
//       password: password
//     })
// }


